class WebtoonModel {
  final String title, thumb, id;

  // name constructor
  WebtoonModel.fromJson(Map<String, dynamic> json)
      : title = json["title"],
        thumb = json["thumb"],
        id = json["id"];
}
